services:
  caddy:
    build: ./caddy
    restart: unless-stopped
    ports:
      - "80:80"
      - "443:443"
      - "443:443/udp"
    environment:
      DOMAIN: ${DOMAIN}
      LE_EMAIL: ${LE_EMAIL}
      CLOUDFLARE_API_TOKEN: ${CLOUDFLARE_API_TOKEN}
    networks:
      tedflix:
        aliases:
          - caddy
    volumes:
      # - ./caddy/Caddyfile:/etc/caddy/Caddyfile
      - caddy_data:/data
      - caddy_config:/config

  organizr:
    image: organizr/organizr:latest
    restart: unless-stopped
    expose:
      - "80"
      - "443"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    volumes:
      - organizr_config:/config
    networks:
      tedflix:
        aliases:
          - home
          - organizr

  jellyfin:
    image: linuxserver/jellyfin:latest
    runtime: nvidia
    restart: unless-stopped
    ports:
      - 8096:8096 # for LAN connection
    expose:
      - "8096" # HTTP
      # - "8920" # HTTPS
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      NVIDIA_DRIVER_CAPABILITIES: all
      NVIDIA_VISIBLE_DEVICES: all
    deploy:
      resources:
        reservations:
          devices:
            - capabilities: [gpu]
    networks:
      tedflix:
        aliases:
          - watch
          - jellyfin
    volumes:
      - jellyfin_config:/config
      - ${DATA_PATH}/media:/data/media
    devices:
      - /dev/dri:/dev/dri

  calibre:
    image: linuxserver/calibre:6.8.0
    restart: unless-stopped
    expose:
      - "8080"
      - "8081"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - calibre
    volumes:
      - calibre_config:/config

  calibreweb:
    image: linuxserver/calibre-web:latest
    restart: unless-stopped
    expose:
      - "8083"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      OAUTHLIB_RELAX_TOKEN_SCOPE: "1"
    networks:
      tedflix:
        aliases:
          - read
          - calibreweb
    volumes:
      - calibreweb_config:/config
      - ${DATA_PATH}/media/books:/books

  ombi:
    image: linuxserver/ombi:latest
    restart: unless-stopped
    expose:
      - "3579"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    volumes:
      - ombi_config:/config
    networks:
      tedflix:
        aliases:
          - request
          - requests
          - ombi

  radarr:
    image: linuxserver/radarr:latest
    restart: unless-stopped
    expose:
      - "7878"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - movie
          - movies
          - radarr
    volumes:
      - radarr_config:/config
      - ${DATA_PATH}:/data

  readarr:
    image: linuxserver/readarr:develop
    restart: unless-stopped
    expose:
      - "7878"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - book
          - books
          - readarr
    volumes:
      - readarr_config:/config
      - ${DATA_PATH}:/data

  sonarr:
    image: linuxserver/sonarr:latest
    restart: unless-stopped
    expose:
      - "8989"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - tv
          - sonarr
    volumes:
      - sonarr_config:/config
      - ${DATA_PATH}:/data

  unpackerr:
    image: hotio/unpackerr:latest
    restart: unless-stopped
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      UN_RADARR_0_URL: http://radarr:7878
      UN_RADARR_0_API_KEY: ${RADARR_API_KEY}
      UN_RADARR_0_PATHS_0: /data/torrent/movies
      UN_RADARR_0_PATHS_1: /data/torrent/movies
      UN_SONARR_0_URL: http://sonarr:8989
      UN_SONARR_0_API_KEY: ${SONARR_API_KEY}
      UN_SONARR_0_PATHS_0: /data/torrent/tv
      UN_SONARR_0_PATHS_1: /data/torrent/tv
    networks:
      tedflix:
        aliases:
          - unpackerr
    volumes:
      - ${DATA_PATH}/torrent:/data/torrent

  unmanic:
    image: josh5/unmanic:latest
    restart: unless-stopped
    runtime: nvidia
    expose:
      - "8888"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      CONFIG_DIR: /config
      LIBRARY_DIR: /data/media
      CACHE_DIR: /tmp/unmanic
      NVIDIA_VISIBLE_DEVICES: all
    networks:
      tedflix:
        aliases:
          - transcode
          - unmanic
    volumes:
      - unmanic_config:/config
      - ${DATA_PATH}/media:/data/media

  jackett:
    image: linuxserver/jackett:latest
    restart: unless-stopped
    expose:
      - "9117"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - jackett
    volumes:
      - jackett_config:/config

  flaresolverr:
    image: ghcr.io/flaresolverr/flaresolverr:latest
    restart: unless-stopped
    expose:
      - "8191"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      # CAPTCHA_SOLVER: hcaptcha-solver # not currently working
    networks:
      tedflix:
        aliases:
          - flaresolverr

  transmission:
    privileged: true
    sysctls:
      - net.ipv6.conf.all.disable_ipv6=0 # https://github.com/haugene/docker-transmission-openvpn/issues/960
    image: haugene/transmission-openvpn:latest
    restart: unless-stopped
    expose:
      - "9091"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
      OPENVPN_PROVIDER: MULLVAD
      OPENVPN_USERNAME: ${MULLVAD_ACCOUNT_NUMBER}
      OPENVPN_PASSWORD: m
      TRANSMISSION_RATIO_LIMIT: "2"
      TRANSMISSION_RATIO_LIMIT_ENABLED: "true"
      TRANSMISSION_DOWNLOAD_QUEUE_ENABLED: "true"
      TRANSMISSION_DOWNLOAD_QUEUE_SIZE: "2000"
      TRANSMISSION_DOWNLOAD_DIR: /data/torrent # Radarr and Sonarr will create movies or tv subdirectory
      TRANSMISSION_INCOMPLETE_DIR: /data/torrent/incomplete
      TRANSMISSION_WATCH_DIR: /data/torrent/watch
    networks:
      tedflix:
        aliases:
          - torrent
          - torrents
          - transmission
    volumes:
      - transmission_config:/config/transmission-home
      - ${DATA_PATH}/torrent:/data/torrent

  nzbget:
    image: linuxserver/nzbget:latest
    restart: unless-stopped
    expose:
      - "6789"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: "America/New_York"
    networks:
      tedflix:
        aliases:
          - usenet
          - nzbget
    volumes:
      - nzbget_config:/config
      - ${DATA_PATH}/usenet:/data/usenet

  # Utilities
  portainer:
    image: portainer/portainer-ce:latest
    restart: unless-stopped
    expose:
      - "9000"
    networks:
      tedflix:
        aliases:
          - portainer
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data

  netdata:
    image: netdata/netdata:latest
    restart: unless-stopped
    expose:
      - "19999"
    networks:
      tedflix:
        aliases:
          - netdata
    volumes:
      - netdata_config:/etc/netdata
      - netdata_lib:/var/lib/netdata
      # - netdata_cache:/var/cache/netdata # Not sure if necessary
      # Read-only monitoring mounts
      - /etc/passwd:/host/etc/passwd:ro
      - /etc/group:/host/etc/group:ro
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /etc/os-release:/host/etc/os-release:ro
    cap_add:
      - SYS_PTRACE
    security_opt:
      - apparmor=unconfined

networks:
  tedflix:
    name: tedflix

volumes:
  caddy_data:
  caddy_config:
  organizr_config:
  jellyfin_config:
  calibre_config:
  calibreweb_config:
  ombi_config:
  radarr_config:
  readarr_config:
  sonarr_config:
  unmanic_config:
  jackett_config:
  transmission_config:
  nzbget_config:
  portainer_data:
  netdata_config:
  netdata_lib:
