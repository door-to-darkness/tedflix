# Door To Darkness - Tedflix

![Tedflix logo](/docs/logo.png)

Tedflix is a full stack media server for today's savvy (digital) cord cutter.

[[_TOC_]]

## Requirements

- Docker
- Cloudflare DNS
- Mullvad VPN
- Usenet access (Newshosting)
- Usenet indexer(s) (NZBGeek)
- Torrent indexer(s) (IPTorrents)

The `docker-compose.yml` file expects the following environment variables:

- `CLOUDFLARE_API_TOKEN`: CloudFlare API token properly scoped to the Zone and its DNS
- `DATA_PATH`: storage for all media server files (/data)
  - Full path or relative to the docker-compose file
- `DOMAIN`: domain from which to serve applications (example.com)
- `LE_EMAIL`: email address for Let's Encrypt notifications (<letsencrypt@example.com>)
- `MULLVAD_ACCOUNT_NUMBER`: Mullvad VPN account number
- `PGID`: Group ID for applications to run as (1000)
- `PUID`: User ID for applications to run as (1000)
- `RADARR_API_KEY`: Radarr API key
- `SONARR_API_KEY`: Sonarr API key

## Usage

```bash
# Bring up the media server
# (assuming the environment variables are set in your shell or in a .env file next to the docker-compose.yml file)
docker compose up -d

# Update images and restart applications
docker compose pull
docker compose up -d
```

Immediately head to `home.${DOMAIN}` and `watch.${DOMAIN}`, even if the reverse proxy is still working on obtaining your certs, as these two services are not behind authentication and need setup. If these applications are exposed to the internet, there's a chance someone configures them before you. It's a slim one, but it could happen.

## Data Structure

```text
data
├── cctv
├── torrent
│  ├── movies
│  ├── music
│  └── tv
├── usenet
│  ├── movies
│  ├── music
│  └── tv
└── media
    ├── movies
    ├── music
    └── tv
```

- `/data/torrent`
  - contains all torrent related data
  - completed downloads should be configured to be in a category in this directory
    - `/data/torrent/movies/The.Departed.2006.720p.BRRip.x264-x0r`
  - Transmission uses this directory to store completed downloads
  - Radarr and Sonarr use this directory to grab completed downloads
  - Unpackerr uses this directory to search for and extract archived completed downloads
- `/data/usenet`
  - contains all usenet related data
  - completed downloads should be configured to be in a category in this directory
    - `/data/usenet/movies/Cape.Fear.1991.BluRay.576p.H264-20-40`
  - NZBGet uses this directory to store completed downloads
  - Radarr and Sonarr use this directory to grab completed downloads
- `/data/media`
  - contains final media
  - organized media should be configured to be in a category in this directory
    - `/data/media/movies/The Departed (2006)/The Departed (2006) Bluray-720p.mkv`
  - Radarr and Sonarr use this directory to organize completed downloads
  - Jellyfin uses this directory to serve organized media

### Data Structure Notes

Some applications may complain about these directories not existing when started from scratch. This script was originally included in the initialization of the Radarr and Sonarr containers, but volume mounting caused complications in the CI/CD pipeline. In any case, just make the directories and ensure they're owned by `abc:abc` inside the containers.

```bash
# Either inside or outside a container
mkdir -p /data/media/movies
mkdir -p /data/media/tv

# Inside a containers
chown abc:abc /data/media/movies
chown abc:abc /data/media/tv
```

## Applications

### Caddy

[Docker Hub](https://hub.docker.com/_/caddy)

Ingress and reverse proxy.

---

### Organizr

[Docker Hub](https://hub.docker.com/r/organizr/organizr)

Central dashboard for all applications, all authorization, and some authentication.

#### Organizr Settings

- Tab Editor
  - Categories
    - Advanced
    - Administration
    - Shortcuts
  - Tabs
    - Dashboard
      - Type: Organizr
    - Watch
      - Tab Url: `https://watch.${DOMAIN}`
    - Read
      - Tab Url: `https://read.${DOMAIN}`
      - Type: New Window
    - Make a Request
      - Tab URL: `https://request.${DOMAIN}/auth/cookie`
    - Radarr (Movies)
      - Tab URL: `https://movies.${DOMAIN}`
      - Category: Advanced
      - Group: Super User
      - Homepage integration:
        - Minimum Authentication: User
        - URL: `http://movies:7878`
        - Token: API Key from `https://movies.${DOMAIN}/settings/general`
        - Queue Minimum Authentication: Power User
        - Queue Add to Combined Downloader: yes
    - Sonarr (TV shows)
      - Tab URL: `https://tv.${DOMAIN}`
      - Category: Advanced
      - Group: Super User
      - Homepage integration:
        - Minimum Authentication: User
        - URL: `http://tv:8989`
        - Token: API Key from `https://tv.${DOMAIN}/settings/general`
        - Queue Minimum Authentication: Power User
        - Queue Add to Combined Downloader: yes
    - Readarr (Books)
      - Tab URL: `https://books.${DOMAIN}`
      - Category: Advanced
      - Group: Super User
    - Calibre (Book Library)
      - Tab URL: `https://calibre.${DOMAIN}`
      - Category: Advanced
      - Group: Admin
    - Jackett (Tracker Proxy)
      - Tab URL: `https://jackett.${DOMAIN}`
      - Category: Advanced
      - Group: Admin
    - Transmission (Torrents)
      - Tab URL: `https://torrent.${DOMAIN}`
      - Category: Advanced
      - Group: Admin
      - Homepage integration:
        - Minimum Authentication: User
        - URL: `http://torrent:9091/transmission`
        - Misc Options
          - Add to Combined Downloader: yes
    - NZBGet (Usenet)
      - Tab URL: `https://usenet.${DOMAIN}`
      - Category: Advanced
      - Group: Admin
      - Homepage integration:
        - Minimum Authentication: User
        - URL: `http://usenet:6789`
        - Misc Options
          - Add to Combined Downloader: yes
    - Settings
      - Category: Advanced
      - Group: Admin
      - Type: Organizr
    - Portainer (Docker management)
      - Tab URL: `https://portainer.${DOMAIN}`
      - Category: Administration
      - Group: Admin
    - Netdata (System monitoring)
      - Tab URL: `https://netdata.${DOMAIN}`
      - Category: Administration
      - Group: Admin
    - UniFi
      - Tab URL: `https://${USG_HOSTNAME}:8443`
      - Category: Administration
      - Group: Admin
      - Type: New Window
    - IPTorrents
      - Tab URL: `https://iptorrents.com`
      - Category: Shortcuts
      - Group: Super User
      - Type: New Window
    - NZBgeek
      - Tab URL: `https://iptorrents.com`
      - Category: Shortcuts
      - Group: Super User
      - Type: New Window
    - Newshosting
      - Tab URL: `https://iptorrents.com`
      - Category: Shortcuts
      - Group: Super User
      - Type: New Window
    - Mullvad VPN
      - Tab URL: `https://iptorrents.com`
      - Category: Shortcuts
      - Group: Super User
      - Type: New Window
    - GitLab - Tedflix
      - Tab URL: `https://iptorrents.com`
      - Category: Shortcuts
      - Group: Super User
      - Type: New Window
- Customize
  - Top Bar
    - Logo: `https://gitlab.com/door-to-darkness/tedflix/-/raw/master/docs/logo.png`
  - Login Page
    - Login Logo: `https://gitlab.com/door-to-darkness/tedflix/-/raw/master/docs/logo.png`
    - Use Logo instead of Title on Login Page: yes
    - Minimal Login Screen: yes
  - Options
    - Disable extra links
  - Notifications
    - Type: `Toastr`
    - Position: `Top Center`
- System Settings
  - Main
    - Authentication
      - Authentication Type: `Organizr DB + Backend`
      - Authentication Backend: `Jellyfin`
      - Jellyfin URL: `http://jellyfin:8096`
      - Jellyfin Token: Use key generated at `https://jellyfin.${DOMAIN}/web/index.html#!/apikeys.html`
    - Login
      - Disable Recover Password
  - Single Sign-On
    - Ombi
      - Ombi URL: `http://request:3579`
      - Token: Ombi api key from `https://request.${DOMAIN}/Settings/Ombi`
    - Jellyfin
      - Jellyfin API URL: `http://jellyfin:8096`
      - Jellyfin SSO URL: `https://jellyfin.${DOMAIN}`

---

### Jellyfin

[Docker Hub](https://hub.docker.com/r/linuxserver/jellyfin/)

Media player.

#### Jellyfin Settings

- Server
  - Libraries
    - Movies
      - `/data/media/movies`
      - Enable real time monitoring
    - TV shows
      - `/data/media/tv`
      - Enable real time monitoring

---

### Calibre

[Docker Hub](https://hub.docker.com/r/linuxserver/calibre)

eBook manager.

#### Calibre Settings

- Preferences
  - Sharing over the net
    - Main
      - Require username and password to access the Content server: yes
      - Run server automatically when calibre starts: yes
    - User accounts
      - Username and password for Readarr

---

### Calibre-Web

[Docker Hub](https://hub.docker.com/r/linuxserver/calibre-web)

Calibre-Web is a web app providing a clean interface for browsing, reading and downloading eBooks using an existing Calibre database.

#### Calibre-Web Settings

- Email Server Settings
  - Gmail SMTP settings
- Edit UI Configuration
  - Adjust settings for New Users

---

### Ombi

[Docker Hub](https://hub.docker.com/r/linuxserver/ombi)

Frontend allowing users to make content requests.

#### Ombi Settings

- Configuration
  - Customization
    - Application Name: `Tedflix`
    - Application Url: `https://request.${DOMAIN}`
- Media Server
  - Jellyfin
    - Hostname: `jellyfin`
    - Port: `8096`
    - Api Key: Use key generated at `https://jellyfin.${DOMAIN}/web/index.html#!/apikeys.html`
    - Externally Facing Hostname: `https://watch.${DOMAIN}`
- TV
  - Sonarr
    - Sonarr Hostname or IP: `sonarr`
    - Port: `8989`
    - Sonarr API Key: API Key from `https://tv.${DOMAIN}/settings/general`
    - Quality Profiles: `Any`
    - Default Root Folders: `/data/media/tv/`
    - Enable season folders
- Movies
  - Radarr
    - Hostname or IP: `radarr`
    - Port: `7878`
    - Sonarr API Key: API Key from `https://movies.${DOMAIN}/settings/general`
    - Quality Profiles: `Any`
    - Default Root Folders: `/data/media/movies/`
    - Enable season folders

---

### Radarr

[Docker Hub](https://hub.docker.com/r/linuxserver/radarr)

Media organizer for movies. Uses torrent and usenet downloaders to get content.

#### Radarr Settings

- Media Management
  - Rename Movies: yes
  - Replace Illegal Characters: yes
  - Colon Replacement: `Replace with Space Dash Space`
  - Unmonitor Deleted Movies: yes
  - Root Folders
    - `/data/media/movies`
- Indexers
  - IPTorrents (Torznab > Custom)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Advanced
      - Seed Ratio: `1`
      - Seed Time: `20161`
  - NZBGeek (Newznab > NZBgeek)
    - API Key: NZBGeek password
- Download Clients
  - NZBGet
    - Host: `nzbget`
    - Port: `6789`
    - Username: ``
    - Password: ``
    - Category: `movies`
  - Transmission
    - Host: `transmission`
    - Port: `9091`
    - Username: ``
    - Password: ``
    - Category: `movies`

---

### Readarr

[Docker Hub](https://hub.docker.com/r/linuxserver/readarr)

Media organizer for books. Uses torrent and usenet downloaders to get content.

#### Readarr Settings

- Media Management
  - Root Folders
    - `/data/media/books`
    - Use Calibre Content Server: yes
    - Host: `calibre`
    - Port: `8081`
    - Calibre Content Server username and password
    - Calibre library: `books`
- Indexers
  - IPTorrents (Torznab)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Advanced
      - Seed Ratio: `1`
      - Seed Time: `20161`
  - NZBGeek (Newznab > NZBgeek)
    - API Key: NZBGeek password
- Download Clients
  - NZBGet
    - Host: `nzbget`
    - Port: `6789`
    - Username: ``
    - Password: ``
    - Category: `books`
  - Transmission
    - Host: `transmission`
    - Port: `9091`
    - Username: ``
    - Password: ``
    - Category: `books`

---

### Sonarr

[Docker Hub](https://hub.docker.com/r/linuxserver/sonarr)

Media organizer for tv shows. Uses torrent and usenet downloaders to get content.

#### Sonarr Settings

- Media Management
  - Rename Episodes: yes
  - Replace Illegal Characters: yes
  - Unmonitor Deleted Episodes: yes
  - Root Folders
    - `/data/media/tv`
- Profiles
  - Quality Profiles
    - Enable upgrades
  - Release Profiles
    - Use [Trash Guides](https://trash-guides.info/Sonarr/V3/Sonarr-Release-Profile-RegEx-Anime/) recommended settings
      - use profile tags `dub` and `dubbed`
- Indexers
  - 1337x (Torznab > Custom)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Categories: `TV/SD TV/HD`
    - Anime Categories: `TV/Anime`
  - Anime Tosho Torrents (Torznab > Custom)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Categories: `5030 5040`
    - Anime Categories: `TV/Anime`
  - Anime Tosho Torrents (Torznab > Custom)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Anime Categories: `TV/Anime Anime`
  - IPTorrents (Torznab > Custom)
    - URL: copy from `https://jackett.${DOMAIN}`, then replace domain in URL with `http://jackett:9117`
    - API Key: copy from `https://jackett.${DOMAIN}`
    - Advanced
      - Seed Ratio: `1`
      - Seed Time: `20161`
      - Season-Pack Seed Time: `20161`
  - NZBGeek (Newznab > NZBgeek)
    - API Key: NZBGeek password
- Download Clients
  - NZBGet
    - Host: `nzbget`
    - Port: `6789`
    - Username: ``
    - Password: ``
    - Category: `tv`
  - Transmission
    - Host: `transmission`
    - Port: `9091`
    - Username: ``
    - Password: ``
    - Category: `tv`

---

### Unpacker

[Docker Hub](https://hub.docker.com/r/hotio/unpackerr)

Unpacks archives from torrents so that Radarr and Sonarr can grab and organize the actual media file, then deletes the extracted file to save space.

#### Unpacker Notes

Since Unpackerr is configured entirely by environment variables, the Radarr and Sonarr API keys will be unknown on first run. Get these keys and provide them in environment variables then remove the container and bring it back up.

---

### Unmanic

[Docker Hub](https://hub.docker.com/r/josh5/unmanic)

Unmanic is a simple tool for optimising your file library. You can use it to convert your files into a single, uniform format, manage file movements based on timestamps, or execute custom commands against a file based on its file size.

---

### Jackett

[Docker Hub](https://hub.docker.com/r/linuxserver/jackett)

Proxy server for Sonarr and Radarr to reach upstream torrent trackers and usenet.

#### Jackett Settings

- Indexers
  - 1337x
  - Anime Tosho
  - AniRena
  - IPTorrents
    - Cookie: cookie including `uid` and `pass` ([how to get cookie](https://github.com/Jackett/Jackett/wiki/Finding-cookies))
- Configuration
  - FlareSolverr API URL: `http://flaresolverr:8191/`

---

### Flaresolverr

[Docker Hub](https://hub.docker.com/r/flaresolverr/flaresolverr/)

Proxy server to bypass Cloudflare protection.

---

### Transmission

[Docker Hub](https://hub.docker.com/r/haugene/transmission-openvpn)

Torrent downloader with OpenVPN.

#### Transmission Notes

- Any changes to container environment variables will only take effect after stopping and removing the container (the variable initialization scripts seem to only run once). Despite this, torrent data should be persisted and downloading and seeding should resume.

---

### NZBGet

[Docker Hub](https://hub.docker.com/r/linuxserver/nzbget)

Usenet downloader.

#### NZBGet Settings

- Paths
  - MainDir: `/data/usenet`
  - DestDir: `${MainDir}`
- News-Servers
  - Server1
    - Name: `Newshosting`
    - Host: `news.newshosting.com`
    - Port: `563`
    - Username: Newshosting username
    - Password: Newshosting password
    - Encryption: `Yes`
    - Connections: `55`
    - Retention: `4610`
- Security
  - Remove `ControlPassword` to disable login prompt. Caddy and Organizr should handle auth.
- Categories
  - Category1 Name: `movies`
  - Category2 Name: `tv`
  - Category3 Name: `music`
  - Category2 Name: `books`

#### NZBGet Notes

Default login:

```text
Login: nzbget
Password: tegbzn6789
```

---

### Portainer

[Docker Hub](https://hub.docker.com/r/portainer/portainer-ce)

Docker management UI.

---

### Netdata

[Docker Hub](https://hub.docker.com/r/netdata/netdata)

Real-time system monitoring dashboard.
